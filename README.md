# Pitch Contour Detection with Harmonic Pursuit

This directory contains an implementation of the Matching Pursuit (MP) using harmonic atoms [Gribonval et al. 2003]. The parameters of these atoms are computed using the Distribution Derivative Method (DDM) [Betser 2009]. 

    .
    ├── README.md
    ├── ddm
    │   └── hmp_hann.m
    ├── gabor
    │   ├── hmp_fof.m
    │   ├── hmp_gabor.m
    │   └── hmp_window_bandwidth.m
    ├── help
    │   └── hmp_default_params.m
    ├── matching
    │   └── hmp_matching.m
    └── pursuit
        └── hmp_pursuit.m

* `ddm` contains the DDM atom parameter estimation methods.
* `gabor` contains the short-term Fourier transform (STFT) methods.
* `help` contains help functions and some default initializers.
* `matching` contains methods for estimating atom parameters.

# References

Betser, M. (2009). Sinusoidal Polynomial Parameter Estimation Using the Distribution Derivative. IEEE Transactions on Signal Processing, 57(12), 4633–4645. https://doi.org/10.1109/TSP.2009.2027401

Gribonval, R., & Bacry, E. (2003). Harmonic decomposition of audio signals with matching pursuit. IEEE Transactions on Signal Processing, 51(1), 101–111. https://doi.org/10.1109/TSP.2002.806592

