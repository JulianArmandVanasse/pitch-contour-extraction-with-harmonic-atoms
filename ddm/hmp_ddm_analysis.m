%% HMP_DDM_ANALYSIS
%
%       Estimates sinusoidal parameters of a component within signal a
%       multicomponent signal.
%
%   Use:
%   
%       ALPHA = HMP_DDM_ANALYSIS(X, Q, H_0, H_1, N_FFT, NUM_NBRS, PK_BIN) estimates
%       the parameters ALPHA of a sinusoidal component that contributes
%       energy to the fft bin PK_BIN. H_0 and H_1 are the window and window
%       derivative signals. N_FFT is the fft size. NUM_NBRS is the number
%       of neighboring atoms used in estimation.
%
%       [ALPHA, FRAME_SIZE, P] = HMP_DDM_ANALYSIS( ... ) does the above and returns the
%       time vector P. This is best for passing to hmp_ddm_synthesis
%
% Julian Vanasse
% McGill University
% 2021-Mar-16

%%
function varargout = hmp_ddm_analysis(x, Q, h_0, h_1, n_fft, num_nbrs, pk_bin)
    %% orient
    if size(h_0, 1) < size(h_0, 2)
        h_0 = h_0';
    end
    if size(h_1, 1) < size(h_1, 2)
        h_1 = h_1';
    end
    if size(x, 1) < size(x, 2)
        x = x';
    end


    %% dimension vars
    frame_size      = length(x);

    %% ddm vars
    n_dft           = (0:n_fft-1)';
    omega           = 2*pi*n_dft/n_fft;
    n_center        = ((frame_size - mod(frame_size,2)) / 2) + 1;
    centering       = exp(1i * omega * n_center);
    
    % time (n)
    n               = (0:frame_size-1)' - n_center;
    i               = (0:Q);
    % polynomial time vector
    p               = bsxfun(@power, n, i);     
    p_d             = bsxfun(@times, bsxfun(@power, n, (0:Q-1)), 1:Q);
    
    %% transform
    % spectrum with window
    spectrum        = fft(x .* h_0, n_fft);
    % spectrum with window derivative
    spectrum_d      = fft(x .* h_1, n_fft);
    
    %% estimate parameters
    % store various ffts 
    Sp              = zeros(n_fft, Q-1);
    % time * signal
    Sp(:, 1)        = spectrum;
    Sp(:, 1)        = Sp(:,1) .* centering;
    for i = 2:Q
        Sp(:, i)        = fft(x .* h_0 .* p_d(:,i), n_fft);
        Sp(:, i)        = Sp(:, i) .* centering;
    end

    % derivative windowed signal
    SD              = spectrum_d .* centering;
    SD              = SD + Sp(:, 1).*(-1i*omega);
    
    % neighborhood
    pbs                 = (pk_bin-num_nbrs:pk_bin+num_nbrs)';
    pbs(pbs < 1)        = [];
    pbs(pbs > n_fft)    = [];

    % define linear equation
    A = Sp(pbs, 1:Q);
    b = -SD(pbs);

    % solve least squares
    alpha           = [0; A\b];
    
    %% output
    varargout{1}    = alpha;
    varargout{2}    = frame_size;
    varargout{3}    = p;
end