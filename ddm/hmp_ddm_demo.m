%% HMP_DDM_DEMO
%
%       Demonstrate sinusoidal parameter estimation for single input.
%
% Julian Vanasse
% McGill University
% 2021-Mar-16
%%

%% system params
frame_size = 1024;
n = 0:frame_size-1;

%% signal
% x = cos(0.11 * n + (0.0001/2 * power(n, 2)));

% x = cos(0.1 * n);
% x = x + 0.1*randn(1, frame_size);
% x = hann(frame_size)' .* x;

%%
load('bug.mat');
x = r_frame;

%% pick peak
X = abs(fft(x));
X = X(1:end/2);
[~, bin] = max(X);

%% ddm params
[win, win_d]    = hmp_hann(frame_size);
Q               = 5;
num_nbrs        = 10;

%% extract parameters
[alpha, frame_size, p] = hmp_ddm_analysis(x, Q, win, win_d, frame_size*2, num_nbrs, bin*2);
alpha

%% synthesize from parameters
[y, c] = hmp_ddm_synthesis(x, alpha, p, Q);

%% plot
plot(n, x, n, y, 'LineWidth', 2)