%% HMP_DDM_SYNTHESIS
%
%       Synthesize sinusoidal component estimated from a signal.
%
%   Use:
%       
%       Y = HMP_DDM_SYNTHESIS(X, ALPHA, FRAME_SIZE, P) 
%
%       [Y, C] = HMP_DDM_SYNTHESIS(X, ALPHA, FRAME_SIZE, P)  where C is the
%       complex correlation between the complex estimated sinusoid and the
%       target signal X.
%
% Julian Vanasse
% McGill University
% 2021-Mar-16
%%
function varargout = hmp_ddm_synthesis(x, alpha, p, Q)
    %% regenerate component
    an = zeros(size(p, 1), 1);
    for i = 1:Q
        an = an + p(:, i) * alpha(i);
    end
    
%     an = 1i*imag(an); % use only imaginary
    
    y = exp(an); % complex exp
    
    %% realignment
    y = y / norm(y, 2);
    re = dot(x, real(y));
    im = dot(x, imag(y));
    y = re*real(y) + im*imag(y);
    y = y*2;
    
    %% output
    varargout{1} = y;
    varargout{2} = re + 1i*im;
end