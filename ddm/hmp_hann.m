%% JAV_HANN
%   
%   [WIN, WIN_D] = JAV_HANN(LEN) returns a hann window in WIN and
%   its derivative computed analytically in WIN_D, both of length LEN.
%
% Julian Vanasse
% McGill University
% 2021-Mar-9

%%
function [win, win_d] = jav_hann(len)

win = hann(len);
t = 1:len;
win_d = 2*sin(t * pi / len) .* cos(t * pi / len) * (pi/len);
win_d = win_d';

end