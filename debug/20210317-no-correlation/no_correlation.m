%% NO_CORRELATION
%
%%
load('selected.mat');

% ddm arguments
% Q               = params.ddm.num_params;
Q               = 3;
h_0             = params.win.signal;
h_1             = params.win.signal_d;
n_fft           = params.specgram.n_fft;
num_nbrs        = params.ddm.num_nbrs;


r_frame         = selected(30).r_frame;

sample_rate     = params.sys.sample_rate;
t               = params.sys.t;
bin             = selected(30).bin;

% estimate 
[ddm_alpha, frame_size, p]  = hmp_ddm_analysis(r_frame, Q, h_0, h_1, n_fft, num_nbrs, bin);

% synthesis
y_1 = hmp_ddm_synthesis(r_frame, ddm_alpha, p, Q);

y_2 = y_1 .* h_0;
%% plot 
clf;
hold on;
plot(r_frame);
plot(y_1);
plot(y_2);
hold off;
