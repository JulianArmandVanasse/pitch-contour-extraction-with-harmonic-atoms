function env = fof(t, a, b)
% FOF Formant wave function.
%
%   ENV = FOF(T, A, B) generates a vector ENV of the formant wave function
%   with parameters A and B.
%
% For details, see Rodet, X. (1980).
%
% Rodet, X. (1980). Time—Domain Formant—Wave—Function Synthesis. In J. C. 
%   Simon (Ed.), Spoken Language Generation and Understanding (pp. 429–441). 
%   Springer Netherlands.
%
%   Julian Vanasse
%   McGill University 
%   Summer, 2020

    C = 1;
    env = (t >= 0 & t < pi/b) .* (0.5 * C * (1-cos(b*t)) .* exp(-a*t)) + ...
            (t >= pi/b & t < 1) .* (C * exp(-a*t));
    env = env / norm(env,2);
    
end
