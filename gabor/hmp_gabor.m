function varargout = hmp_gabor(x, specgram, win, sys)
% HMP_GABOR Gabor frame decomposition, i.e. short-time Fourier transform.
%
%   Y = HMP_GABOR(X, SPECGRAM, WINDOW, SYS) returns the Gabor frame decomposition
%   for signal vector X, using parameters from SPECGRAM and WINDOW structs.
%      
%       X                   : audio signal                      (vector)
%       SPECGRAM            : spectrogram parameters            
%           .n_fft          : fft size                          (integer)
%           .hop_size       : temporal offset                   (integer)
%       WIN                 : window parameters                 
%           .signal         : window vector                     (vector)
%       SYS                 : system                
%           .sample_rate    : sample rate                       (integer)
%
%   [Y, T, W] = HMP_GABOR(X, SPECGRAM, WINDOW, SYS) returns the Gabor frame with
%   time vector T and frequency vector W. The frequency vector is
%   normalized such that 1 is assigned to the Nyquist frequency. 
%   
%
%   Julian Vanasse
%   McGill University 
%   Summer, 2020


    % move parameters to local variables
    n_fft           = specgram.n_fft;
    hop_size        = specgram.hop_size;
    win             = win.signal;
    window_len      = length(win);
    sample_rate     = sys.sample_rate;

    % zero pad up to multiple of n_fft
    x_len           = length(x);
    y               = zeros((ceil(x_len / n_fft) + 1) * n_fft, 1);
    y(1:x_len)      = x;
    x               = y;
    
    % short-time Fourier transform
    m = 1;
    for n = 1:hop_size:(x_len + hop_size)
        % apply window
        s       = x(n:n+window_len-1);
        s       = s .* win;
        s       = fft(s, n_fft);
        
        % take only positive frequencies
        Y(:,m)  = s(1:(n_fft/2 + 1));
        
        % advance
        m       = m + 1;
    end
    
    % time vector
    [num_w, num_t]      = size(Y);
    dur                 = x_len / sample_rate;
    t                   = linspace(0, dur, num_t);
    
    % frequency vector
    w                   = linspace(0, 0.5, num_w);
    
    % assign to output
    varargout{1}        = Y;
    varargout{2}        = t;
    varargout{3}        = w;
end