function bw = window_bandwidth(atom)
% WINDOW_BANDWIDTH find bandwidth of a bump function.
%
%   BW = WINDOW_BANDWIDTH(ATOM) returns the number of frequency bins
%   greater than -3dB.
%
%
%   Julian Vanasse
%   McGill University 
%   Summer, 2020

    A = fftshift(fft(atom));
    A = abs(A);
    A = 20*log10(A);

    bw = sum(A > -3);
end