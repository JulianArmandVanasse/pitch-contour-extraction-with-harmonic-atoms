function params = #hmp_default_params(audio)
%% matching pursuit
num_iterations = 400;

%% system settings
sys.sample_rate = 1;
sys.t = 0:length(audio)-1;
%% window 
window_size         = 1024;
[h_0, h_1]          = hann_and_derivative(window_size);
h_0_norm            = norm(h_0, 2);

window.size         = window_size;
window.signal       = h_0; 
window.signal       = window.signal / h_0_norm;
%% gabor decomposition
specgram.n_fft      = length(window.signal) * 4;
specgram.hop_size   = 256;
%% search
% search.bin_range    = [5, specgram.n_fft/2];
search.bin_range        = [3, specgram.n_fft];
search.num_pks          = 10;
search.num_partials     = 3;
search.relaxation       = 1;
search.num_iterations   = num_iterations;
%% ddm
search.ddm.num_params   = 4;
search.ddm.h_0          = window.signal;
search.ddm.h_1          = h_1 / h_0_norm;
search.ddm.n_fft        = specgram.n_fft;
search.ddm.num_nbrs     = 10;
%% ======== nonredundancy data ========== %%
% computed and updated with each iteration of the pursuit.
decomposition.tf            = [];
decomposition.candidates    = [];
decomposition.recomp_bounds = [];
decomposition.num_w         = [];
decomposition.num_t         = [];

%% encapsulate in one struct
params.sys              = sys;
params.search           = search;
params.window           = window;
params.specgram         = specgram;
params.decomposition    = decomposition;