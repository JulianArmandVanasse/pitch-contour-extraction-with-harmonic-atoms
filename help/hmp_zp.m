%% HMP_ZP
%
%       Zero pad.
%
%   Use:
% 
%       Y = HMP_ZP(X, M) appends M zeros to the front AND back of vector X.
%
%       Y = HMP_ZP(X, M, LOC) appends M zeros to side of vector X indicaded
%       by LOC = 'front' or 'back'.
%
%       Note: return orientation is [LEN, 1]
%
%   Example:
%
%       y = HMP_ZP([1 2 3 4 5], 3, 'front');
%
% Julian Vanasse
% McGill University
% 2021-Mar-17
%%

function y = hmp_zp(varargin)
    %% args
    x   = varargin{1};
    m   = varargin{2};
    loc = '';
    
    if (nargin == 3)
        loc = varargin{3};
    end
    if (~isvector(x))
        error('hmp_zp: x must be a vector');
    end
    
    %% vars
    len = length(x);
    
    %% append zeros
    if (isempty(loc))
        y = zeros(2*m + len, 1);
        y(m+1:m+len) = x;
    elseif (strcmp(loc,'front'))
        y = zeros(m + len, 1);
        y(m+1:m+len) = x;
    elseif (strcmp(loc,'back'))
        y = zeros(m + len, 1);
        y(1:len) = x;
    else
        error('hmp_zp: loc is not supported'); 
    end
end