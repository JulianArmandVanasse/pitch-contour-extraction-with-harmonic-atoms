%% HMP_DDM_PARAM
%
%   Generate sinusoidal parameter estimation (DDM) params [Betser, 2009].
%
%   Use: 
%       DDM = HMP_DDM_PARAM() generates DDM with default values.
%
%       DDM = HMP_DDM_PARAM(NUM_PARAMS, NUM_NBRS) assigns number of
%       parameters (Q in [Betser, 2009]) to NUM_PARAMS and number of atoms 
%       to aid estimation NUM_NBRS
%
%   Example:
%   
%       ddm = HMP_DDM_PARAM(3, 10)
%
% References:
% 
% Betser, M. (2009). Sinusoidal Polynomial Parameter Estimation Using the 
%   Distribution Derivative. IEEE Transactions on Signal Processing, 
%   57(12), 4633–4645. https://doi.org/10.1109/TSP.2009.2027401
%
%
%
% Julian Vanasse
% McGill University
% 2021-Mar-15
%%
function ddm = hmp_ddm_param(varargin)

%% check args
num_params      = 3;    % Q in [Betser, 2009]
num_nbrs        = 5;   
if (nargin == 2)
    num_params      = varargin{1};
    num_nbrs        = varargin{2};
end

%% ddm
ddm.num_params          = num_params;
ddm.num_nbrs            = num_nbrs;
end