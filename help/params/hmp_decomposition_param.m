%% HMP_DECOMPOSITION_PARAM
%
%   Generate sinusoidal parameter estimation (DDM) params [Betser, 2009].
%
%   Use: 
%       DECOMPOSITION = HMP_DECOMPOSITION_PARAM() generates DECOMPOSITION
%       struct with empty values.
%
%
%   Example:
%   
%       decomposition = HMP_DECOMPOSITION_PARAM();
%
% Julian Vanasse
% McGill University
% 2021-Mar-16
%%
function decomposition = hmp_decomposition_param()
    
decomposition.tf            = [];
decomposition.candidates    = [];
decomposition.recomp_bounds = [];
decomposition.num_w         = [];
decomposition.num_t         = [];

end