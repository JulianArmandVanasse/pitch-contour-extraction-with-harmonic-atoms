%% HMP_DEFAULT_PARAMS
%
%       Generates default params struct.
%
%   Use: 
%       
%       PARAMS = HMP_DEFAULT_PARAMS(SYS)
% 
% Julian Vanasse
% McGill University
% 2021-Mar-16
%%

function params = hmp_default_params(sys)

%% window 
win             = hmp_win_param();
%% gabor decomposition
specgram        = hmp_specgram_param();
%% search
search          = hmp_search_param();
%% ddm
ddm             = hmp_ddm_param();
%% nonredundancy 
decomposition   = hmp_decomposition_param(); 

%% encapsulate in one struct
params.sys              = sys;
params.search           = search;
params.win              = win;
params.specgram         = specgram;
params.ddm              = ddm;
params.decomposition    = decomposition;

end