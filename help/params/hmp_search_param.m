%% HMP_SEARCH_PARAM
%
%   Generate atom search params and number of iterations.
%
%   Use: 
%       SEARCH = HMP_SEARCH_PARAM() generates search with default values.
%
%       SEARCH = HMP_SEARCH_PARAM(BIN_RANGE, NUM_PKS, NUM_PARTIALS, RELAX,
%       NUM_ITERATIONS) generates struct SEARCH with those parameters.
%
%   Example:
%   
%       search = HMP_SEARCH_PARAM([1, 250], 10, 5, 1, 100);
%
% Julian Vanasse
% McGill University
% 2021-Mar-15

%% 
function search = hmp_search_param(varargin)

%% check args
bin_range       = [10, 1024];
num_pks         = 10;
num_partials    = 5;
relaxation      = 1;
num_iterations  = 100;

if (nargin == 5)
    bin_range       = varargin{1};
    num_pks         = varargin{2};
    num_partials    = varargin{3};
    relaxation      = varargin{4};
    num_iterations  = varargin{5};
end

%% search
search.bin_range        = bin_range;
search.num_pks          = num_pks;
search.num_partials     = num_partials;
search.relaxation       = relaxation;
search.num_iterations   = num_iterations;

end