%% HMP_SPECGRAM_PARAM
%
%   Generate gabor decomposition params.
%
%   Use: 
%       SPECGRAM = HMP_SPECGRAM_PARAM() generates default values for
%       param.
% 
%       SPECGRAM = HMP_SPECGRAM_PARAM(N_FFT, HOP_SIZE) generates param with
%       N_FFT and HOP_SIZE.
%
%   Example:
%   
%       specgram = hmp_specgram_param(2048, 256)
%
% Julian Vanasse
% McGill University
% 2021-Mar-15

%%
function specgram = hmp_specgram_param(varargin)

%% check args
% defaults
n_fft       = 2048;
hop_size    = 256;
% else
if (nargin == 2)
    n_fft       = varargin{1};
    hop_size    = varargin{2};
end

%% specgram
specgram.n_fft          = n_fft;
specgram.hop_size       = hop_size;

end