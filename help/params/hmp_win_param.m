%% HMP_WIN_PARAM
%
%   Generate window param.
%
%   Hann window type.
%
%   Use: 
%       WIN = HMP_SPECGRAM_PARAM() generates default values WIN
%
%       WIN = HMP_SPECGRAM_PARAM(SIZE) generates a Hann window with size
%       SIZE.
%
%   Example:
%   
%       win = hmp_win_param(2048);
%
% Julian Vanasse
% McGill University
% 2021-Mar-15
%%

function win = hmp_win_param(varargin)
%% check args
size = 1024;
if (nargin == 1)
    size = varargin{1};
end

%% window
[win.signal, win.signal_d]      = hmp_hann(size);

win.size                        = size;
% win.signal                      = win.signal / norm(win.signal, 2);
% win.signal_d                    = win.signal_d / norm(win.signal, 2);
win.signal                      = win.signal;
win.signal_d                    = win.signal_d;

end