function y = harmonic_chirp(t, f0, t1, f1, h, method)

y = chirp(t, f0, t1, f1, method);

for k = 2:h
    y = y + chirp(t, k*f0, t1, k*f1, method);
end

end