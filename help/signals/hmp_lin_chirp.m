%% LIN_CHIRP
%
%   [CH, F_T] = LIN_CHIRP(T, F1, F2, NUM_PARTIALS)
%
%   F_T is the intantaneous frequency of the funamental of a linear chirp.
%
%   CH is oriented with size(CH) = [LEN, 1]

%%
function [ch, f_t] = lin_chirp(t, f1, f2, num_partials)

%% length
len = length(t);
f_t = zeros(len, 1);

%% compute angular integral
dn = t(2)-t(1);
f_t(:,1) = linspace(f1, f2, len);
angle = 2*pi * f_t .* dn;
angle = cumtrapz(angle);

%% loop and add partials
ch = zeros(len, 1);
for k = 1:num_partials
    ch(1:len) = ch(1:len) + cos(k * angle);
end

end