%% HMP_VIBRATO_CHIRP
%
%   [CH, F_T] = VIBRATO_CHIRP(T, F_C, F_M, NUM_PARTIALS)
%
%   F_T is the instantaneous frequency of the fundamental of a cosine
%   at frequency F_C, modulated by a frequency F_M.
%
%   CH is oriented with size(CH) = [LEN, 1]
%
% Julian Vanasse
% McGill University
% 2021-Jan-12
%%
function [ch, f_t] = hmp_vibrato_chirp(t, f_c, f_m, intensity, num_partials)
    %% length
    len     = length(t);
    f_t     = zeros(len, 1);
    
    %% compute if
    dn          = t(2)-t(1);
    f_t(:,1)    = f_c + intensity*cos(2*pi*f_m*t);
    f_t         = f_t;
    
    %% integrate
    angle       = cumtrapz(2*pi*dn * f_t);
    
    %% loop and add partials
    ch          = zeros(len, 1);
    for k = 1:num_partials
        ch(1:len) = ch(1:len) + cos(k * angle);
    end
end