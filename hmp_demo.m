%% HMP_DEMO
%
%   Example use of HMP.
%
%   Run HMP_DEMO from HMP-F0-DETECTION_v1 directory.
%
% Julian Vanasse
% McGill University
% 2021-Mar-15
%% 

%% add scripts
addpath(genpath('.'));

%% system settings
sys.sample_rate         = 1;
sys.len                 = 8000;
sys.t                   = linspace(0, sys.len/sys.sample_rate, sys.len);

%% default params
params = hmp_default_params(sys);

%% generate audio
[audio, f_i]            = hmp_vibrato_chirp(sys.t, 0.1, 0.0003, 0.001, 3);
% [audio, f_i]            = hmp_lin_chirp(sys.t, 0.1, 0.1, 1);


%% plot
[tf.tf, tf.t, tf.f]     = hmp_gabor(audio, params.specgram, params.win, params.sys);

clf;
hold on;
imagesc(tf.t, tf.f, 20*log10(abs(tf.tf)));
set(gca,'YDir','normal');
plot(sys.t-(2*params.specgram.hop_size/params.sys.sample_rate), f_i, 'LineWidth', 2);
xlim([params.sys.t(1), params.sys.t(end)]);
ylim([tf.f(1), tf.f(end)]);
hold off;

%% matching pursuit
% params change
params.specgram.n_fft   = 2048;
params.ddm.num_params   = 3;
params.ddm.num_nbrs     = 4;
params.search.num_iterations = 1000;

% zero pad
audio           = hmp_zp(audio, params.specgram.n_fft * 4);
params.sys.t    = hmp_zp(params.sys.t, params.specgram.n_fft * 4);

[r, s, selected] = hmp_pursuit(audio, params);
