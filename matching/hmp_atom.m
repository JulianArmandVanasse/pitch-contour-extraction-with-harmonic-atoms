%% HMP_ATOM
%
%   Generates harmonic atom.
%
%   Algorithm:
%   1. Calculate sinusoidal parameters (DDM)
%   2. Generate atom from parameters
%
% Julian Vanasse
% McGill University
% 2021-Mar-15

%%
function selected = hmp_atom(selected, spectrum, r, params)
  
%% local vars
sys             = params.sys;
search          = params.search;
specgram        = params.specgram;
win             = params.win;

sample_rate     = sys.sample_rate;
t               = sys.t;
bin             = selected.bin;

delay           = ((selected.frame-1)*specgram.hop_size);
delay_t         = delay / sample_rate;
atom_t          = (0:(win.size-1)) * (1/sample_rate);                

%% calculate fundamental sinusoidal parameters (ddm_params)
% ddm arguments
Q               = params.ddm.num_params;
h_0             = params.win.signal;
h_1             = params.win.signal_d;
n_fft           = params.specgram.n_fft;
num_nbrs        = params.ddm.num_nbrs;

% ddm signal
i               = (delay+1:(delay+win.size));
r_frame         = r(i) .* win.signal;
% r_frame         = r(i);

% estimate 
[ddm_alpha, frame_size, p]  = hmp_ddm_analysis(r_frame, Q, h_0, h_1, n_fft, num_nbrs, bin);

                
%% malloc
atom            = zeros(length(t), 1);
subatom         = zeros(length(t), 1);

%% harmonic loop --- ENCAPSULATE
k = 1;
w0 = imag(ddm_alpha(2));   % fundamental frequency
while ((k*w0) < (2*pi)) && (k <= search.num_partials)
    
    %% synthesize sinusoidal parameter
    [ddm_est, c]    = hmp_ddm_synthesis(r_frame, 1i*imag(ddm_alpha) * k, p, Q);
    % apply window
    ddm_est = ddm_est .* win.signal;
    ddm_est         = ddm_est / norm(ddm_est, 2);
    subatom(i)      = ddm_est; 
%     subatom(i) = ddm_est / max(abs(ddm_est));
    
%     c          = dot(real(subatom), r);
    c           = dot(ddm_est, r_frame);

    subatom         = c * subatom;
    atom            = atom + subatom;
    
    selected.c(k)   = c;
    selected.f0(k)  = abs(w0*k*sys.sample_rate) / (2*pi);
    
    
    k = k+1;
    
end

%% store
selected.ddm_params     = ddm_alpha;
selected.atom           = atom;
selected.r_frame        = r_frame;


end