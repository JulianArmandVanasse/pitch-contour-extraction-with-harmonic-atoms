%% HMP_HEURISTIC
%
%   Estimate atom fundamental frequency and position by naive summing of
%   harmonically related ft bins.
%
% Julian Vanasse
% McGill University
% 2021-Mar-15

%%
function e = hmp_heuristic(spectrum, pks, bin, search)
    
%% malloc energy
num_pks     = length(pks);
e           = zeros(num_pks, 1);

%% search with relaxation around peaks
d           = search.relaxation-1;
for p = 1:num_pks
    
    h_bin       = bin(p):(bin(p)-1):length(spectrum);
    if length(h_bin) >= search.num_partials
        h_bin       = h_bin(1:search.num_partials);
    end
    h_bin           = h_bin(h_bin >= search.bin_range(1));
    h_bin           = h_bin(h_bin <= search.bin_range(2));
    
    for h = 1:length(h_bin)
        i           = (h_bin(h)-d):(h_bin(h)+d);
        i           = i(i >= search.bin_range(1));
        i           = i(i <= search.bin_range(2));

        e(p)        = e(p) + sum(spectrum(i));
    end
end

end