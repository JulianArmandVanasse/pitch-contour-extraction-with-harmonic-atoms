%% MATCHING
%
%   Matching component of the Matching Pursuit. 
%
%   [SELECTED, R, S, DECOMPOSITION] = MATCHING(R, S, PARAMS)
%       
%       Where R is the residual vector, S is the synthesized vector, PARAMS
%       is the search parameter structure.
%
%       Returns SELECTED, the struct array of selected atom parameters, R
%       is the residual vector with most recent atom removed, S is the
%       synthesized vector with atom added, DECOMPOSITION is the
%       time-frequency decomposition structure to be passed to the next
%       matching cycle.
%
% Julian Vanasse
% McGill University
% 2021-Mar-9
%%

function [selected, r, s, decomposition] = hmp_matching(r, s, params)

% copy mp params to local struct
specgram        = params.specgram;
window          = params.win;
search          = params.search;
sys             = params.sys;
decomposition   = params.decomposition;

% system settings
sample_rate     = sys.sample_rate;
t               = sys.t;
len             = length(t);

% compute initial gabor decomposition
if isempty(decomposition.tf)
    
    tf              = hmp_gabor(r, specgram, window, sys);
    [num_w, num_t]  = size(tf);
    
    decomposition.tf            = tf;
    decomposition.num_w         = num_w;
    decomposition.num_t         = num_t;
    decomposition.recomp_bounds = [1 num_t];
else 
    %% project atoms altered last iteration
    frames          = decomposition.recomp_bounds(1):decomposition.recomp_bounds(end);
    num_frames      = length(frames);
    st               = ((frames(1)-1)*specgram.hop_size)+1;
    en               = st + ((num_frames) * specgram.hop_size);
    tf              = hmp_gabor(r(st:en), specgram, window, sys);
    
    decomposition.tf(:, frames) = tf(:, 1:num_frames);
    imagesc(abs(decomposition.tf)); set(gca, 'YDir', 'normal');
    
end

% search through spectral peaks
bins            = cell(decomposition.num_t);
for n = decomposition.recomp_bounds(1):decomposition.recomp_bounds(end)
    % all candidate atoms at time-frame n
    spectrum        = decomposition.tf(:, n);
    % find peaks 
    [pks, bin]      = findpeaks(abs(spectrum));
    % remove out-of-bounds peaks
    in_range        = (bin >= search.bin_range(1) & bin <= search.bin_range(2));
    bin             = bin(in_range);
    pks             = pks(in_range);
    % sort and truncate
    [pks, i]        = sort(pks, 'descend');
    bin             = bin(i);
    if length(pks) > search.num_pks
        pks             = pks(1:search.num_pks);
        bin             = bin(1:search.num_pks);
    end
    % find energy of peaks and their partials
    e               = hmp_heuristic(abs(spectrum), pks, bin, search);
    % find maximum partial energy at time n
    [m, i]              = max(e);
    decomposition.candidates.bins{n}  = bin(i);
    decomposition.candidates.e{n}     = m;
    if ~isempty(decomposition.candidates.e{n})
        decomposition.candidates.frame{n} = n;
    end
end

% vectorize
candidates.bins     = cell2mat(decomposition.candidates.bins(:));
candidates.e        = cell2mat(decomposition.candidates.e(:));
candidates.frame    = cell2mat(decomposition.candidates.frame(:));
% select max harmonic energy
[~, i]              = max(candidates.e);
selected.bin        = candidates.bins(i);
selected.frame      = candidates.frame(i);

%% store analysis for next iteration
overlap             = ceil(window.size / specgram.hop_size);
decomposition.recomp_bounds = [selected.frame-overlap selected.frame+overlap];
% decomposition.candidates = candidates;

%% generate atom
selected.f0 = [];
selected.ph = [];
selected.window_size = window.size;
selected.hop_size = specgram.hop_size;

selected    = hmp_atom(selected, decomposition.tf(:, selected.frame), r, params);

%% apply pursuit
atom = selected.atom;
r = r - atom;
s = s + atom;

end
