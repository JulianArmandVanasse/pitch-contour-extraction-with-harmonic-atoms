%% PURSUIT
%
%   Pursuit part of Matching Pursuit. 
%
%   [R, S, SELECTED] = PURSUIT(X, PARAMS)
%
%       Where X is an audio vector

function [r, s, selected] = hmp_pursuit(x, params)

%% initialize residue and synthesized signals
r = x;
s = zeros(length(x), 1);

%% matching pursuit
residual = 0.0;
init_rms = rms(x);

%% --- iterate 
for i = 1:params.search.num_iterations
    
    %% --- match
    [sel, r, s, params.decomposition] = hmp_matching(r, s, params);

    selected(i) = sel;
    
    %% --- report residual
    residual(i+1) = 20*log10(rms(r)/init_rms);
    fprintf("i = %d \t %.24f \n", i, residual(i+1));
    if (residual(i+1)-residual(i)) == 0
        
        warning("hmp_pursuit: selected atom with no correlation.");
        disp(selected(i))   
        
        break;
    end
end